package com.example.lotto.controller;

import com.example.lotto.Generator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LottoController {

    @ResponseBody
    @GetMapping("/hello")
    public String hello(){
        return "<h1>Hello World</h1>";
    }
    @GetMapping("/lotto")
    public String number(ModelMap modelMap){
        modelMap.put("numbers", Generator.number());
        return "Lotto"; // zwróć mi htmla o nazwie gome , który ma się znajdować
        // w katalogu template co oznacz zwó©ć resources/templates/home.html
    }

}
